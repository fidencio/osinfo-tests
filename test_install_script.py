import datetime
import os.path
import shutil
import subprocess

import pytest


OSINFO_INSTALL_SCRIPT = shutil.which('osinfo-install-script')
if not OSINFO_INSTALL_SCRIPT:
    pytest.skip('osinfo-install-script not installed', allow_module_level=True)

OSINFO_QUERY = shutil.which('osinfo-query')
if not OSINFO_QUERY:
    pytest.skip('osinfo-query not installed', allow_module_level=True)

KSVALIDATOR = shutil.which('ksvalidator')
if not KSVALIDATOR:
    pytest.skip('ksvalidator not installed', allow_module_level=True)


def build_ksvalidator_arg(shortid, version):
    argument = None

    # Fedora / Silverblue
    if 'fedora' in shortid or 'silverblue' in shortid:
        if version == 'rawhide' or version == 'unknown':
            return 'DEVEL'
        argument = 'F' + version

    # RHEL / CentOS
    if 'centos' in shortid or 'rhel' in shortid:
        argument = 'RHEL'

        if 'unknown' in version:
            if '6' in version:
                argument += '6'
            elif '7' in  version:
                argument += '7'
            elif '8' in version:
                argument += '8'
        else:
            if '6.' in version or version == '6':
                argument += '6'
            elif '7.' in version or version == '7':
                argument += '7'
            elif '8.' in version or version == '8':
                argument += '8'

    return argument

OSINFO_INFO_MAPPING = []
for line in subprocess.check_output(
    [OSINFO_QUERY,
     'os',
     '--fields',
     'short-id,version,release-date,eol-date'],
    universal_newlines=True,
).splitlines()[2:]:
    # short-id, version, release_date, and eol_date are the fields
    # we are interested in from osinfo-query os.
    shortid, version, release, eol = [s.strip() for s in line.split('|')]

    # Only store info for Fedora / Silverblue / CentOS / RHEL.
    if (not 'fedora' in shortid and
        not 'silverblue' in shortid and
        not 'centos' in shortid and
        not 'rhel'in shortid):
        continue

    # Only store OSes which did not reach their EOL.
    today = datetime.date.today()
    if (eol and
        datetime.datetime.strptime(eol, '%Y-%m-%d').date() < today):
        continue

    # Only store OSes which have install-scripts support.
    profiles = [
        line.strip().split(': ')[0] for line in subprocess.check_output(
            [OSINFO_INSTALL_SCRIPT,  '--list-profiles', shortid],
            universal_newlines=True,
        ).splitlines()
    ]
    if not profiles:
        continue

    ksvalidator_arg = build_ksvalidator_arg(shortid, version)

    for profile in profiles:
        OSINFO_INFO_MAPPING.append([shortid, ksvalidator_arg, profile])


@pytest.mark.parametrize('shortid,ksvalidator_arg,profile', OSINFO_INFO_MAPPING)
def test_kickstart_validate(shortid, ksvalidator_arg, profile, tmpdir):
    for source in ['media', 'network']:

        # As osinfo-db does not have URL information about RHEL
        # unattended installations, let's just avoid testing it.
        if source == 'network' and 'rhel' in shortid:
            continue

        filename = subprocess.check_output(
            [OSINFO_INSTALL_SCRIPT,
             '--output-dir', tmpdir,
             '--profile', profile,
             '--installation-source', source,
             '--config', 'user-login=test',
             '--config', 'target-disk=/dev/vda',
             '--config', 'script-disk=/dev/sda',
             '--config', 'l10n-timezone=Europe/Prague',
             '--config', 'l10n-language=en_US',
             '--config', 'l10n-keyboard=in (eng)',
             '--config', 'user-password=password',
             '--config', 'admin-password=password',
             shortid],
            universal_newlines=True,).strip()

        res = subprocess.check_call([
            KSVALIDATOR,
            '-v', ksvalidator_arg,
            os.path.join(tmpdir, filename)])

        assert(res == 0)
