import os
import shutil
import subprocess

import pytest

VIRT_INSTALL = shutil.which('virt-install')
if not VIRT_INSTALL:
    pytest.skip('virt-install not installed', allow_module_level=True)

GUESTFISH = shutil.which('guestfish')
if not GUESTFISH:
    pytest.skip('guestfish not installed', allow_module_level=True)


@pytest.mark.parametrize('os_name', [
    'fedora30',
    'fedora31',
    'centos8'
])
def test_install_successful(os_name):
    name = 'osinfo-test-' + os_name
    image_path = os.path.expanduser(
        '~/.local/share/libvirt/images/{}.qcow2'.format(name)
    )
    output = ''
    try:
        subprocess.check_call([
            VIRT_INSTALL,
            '--name', name,
            '--disk', 'size=10',
            '--install', os_name,
            '--unattended',
            '--wait', '1800',
            '--noreboot',
        ])
        output = subprocess.check_output(
            [GUESTFISH,
             '--ro',
             '-a', image_path,
             '-i',
             'is-file', '/root/osinfo-install-successful', 'followsymlinks:true'],
            env=dict(os.environ, LIBGUESTFS_BACKEND='direct'),
            universal_newlines=True,
        )
    finally:
        subprocess.call(['virsh', 'destroy', name])
        subprocess.call(['virsh', 'undefine', name])
        try:
            os.remove(image_path)
        except FileNotFoundError:
            pass
    assert output.strip() == 'true'
